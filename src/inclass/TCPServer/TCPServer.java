import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

public class TCPServer {
    static AtomicInteger total = new AtomicInteger(0);
    static class BadThread extends Thread {
        Integer i = 0;
        BadThread(Integer i) {
            this.i = i;
        }
        public void run() {
            total.getAndAdd(i);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        int sum = 0;
        for (int i = 0; i < 10_000; i++) {
            sum += i;
        }
        System.out.println("sum of first 100,000 numbers is " + sum);
        System.out.println("my id is " + Thread.currentThread().getId());
        ArrayList<BadThread> threads = new ArrayList<>();
        for (int i = 0; i < 10_000; i++) {
            BadThread bad = new BadThread(i);
            threads.add(bad);
            bad.start();
        }

        for (BadThread bad: threads) {
            bad.join();
        }
        System.out.println("everything done");
        System.out.println("thread total = " + total.get());

        /*
        try {
            ServerSocket ss = new ServerSocket(2222);
            Socket sock = ss.accept();
            System.out.println(sock);
            sock.getOutputStream().write("hello\n".getBytes());
            byte[] bytes = new byte[100];
            sock.getInputStream().read(bytes);
            System.out.println(new String(bytes));
            Socket sock2 = ss.accept();
            System.out.println(sock2);
            sock2.getOutputStream().write("hello2\n".getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

         */
    }
}
