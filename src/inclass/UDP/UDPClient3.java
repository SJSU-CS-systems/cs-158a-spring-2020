package inclass.UDP;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.*;
import java.nio.ByteBuffer;

public class UDPClient3 {
    public static void main(String args[]) {
        try {
            DatagramSocket dsock = new DatagramSocket();
            System.out.println(dsock.getLocalSocketAddress());
            //dsock.connect(InetAddress.getByName("cs-reed-01.cs.sjsu.edu"), 1234);
            byte[] bytes = new byte[1024];
            DatagramPacket packet = new DatagramPacket(bytes, bytes.length);
            packet.setAddress(InetAddress.getByName("cs-reed-01.cs.sjsu.edu"));
            packet.setPort(1234);
            dsock.send(packet);
            packet.setLength(bytes.length);
            dsock.receive(packet);
            for (int i = 0; i < packet.getLength(); i++) {
                System.out.printf("%d: %x\n", i, bytes[i]);
            }
            System.out.println(new String(packet.getData(), 2, packet.getLength()-2));
            ByteArrayInputStream bais = new ByteArrayInputStream(packet.getData());
            DataInputStream dis = new DataInputStream(bais);
            short type = dis.readShort();
            System.out.println("type is " + type);
            ByteBuffer bb = ByteBuffer.wrap(packet.getData());
            System.out.println("bb type is " + bb.getShort());
            System.out.println("bb type is " + bb.getShort());
            System.out.println("starting receive");
            dsock.receive(packet);
            System.out.println("finished receive");
            for (int i = 0; i < packet.getLength(); i++) {
                System.out.printf("%d: %x\n", i, bytes[i]);
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
