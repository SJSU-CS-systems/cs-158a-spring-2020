package inclass.UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UDPServer2 {
    public static void main(String args[]) {
        try {
            DatagramSocket dsock = new DatagramSocket(2222);
            byte[] buffer = new byte[1024];
            while (true) {
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                dsock.receive(packet);
                System.out.println("got: " + new String(packet.getData()) + " from " + packet.getSocketAddress());
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
