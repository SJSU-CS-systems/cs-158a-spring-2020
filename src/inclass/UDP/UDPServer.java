package inclass.UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UDPServer {
    public static void main(String args[]) {
        try {
            DatagramSocket dsock = new DatagramSocket(2222);
            byte[] buffer = new byte[300];
            while (true) {
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                dsock.receive(packet);
                System.out.println("got: " + new String(packet.getData(), 0, packet.getLength()) +
                        " from " + packet.getSocketAddress());
                byte[] message = "hello".getBytes();
                packet = new DatagramPacket(message, 0, message.length, packet.getAddress(),
                        packet.getPort());
                dsock.send(packet);
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
