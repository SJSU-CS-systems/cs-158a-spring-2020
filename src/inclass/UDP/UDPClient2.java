package inclass.UDP;

import java.io.IOException;
import java.net.*;

public class UDPClient2 {
    public static void main(String args[]) {
        try {
            DatagramSocket dsock = new DatagramSocket();
            byte[] bytes = new byte[23533];
            for (int i = 0; i < bytes.length; i++) {
                bytes[i] = (byte)(i / 256);
            }
            DatagramPacket packet = new DatagramPacket(bytes, bytes.length);
            packet.setAddress(InetAddress.getByName("cs-reed-01.cs.sjsu.edu"));
            packet.setPort(2222);
            dsock.send(packet);
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
